<!DOCTYPE html>
<html>
<head>
    <title>Percobaan PaaS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/my-css.css">
</head>
<body>
	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-md justify-content-center">
		<div class="container-fluid">
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#wrap" aria-controls="wrap" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
			</button>
		  	<div class="collapse navbar-collapse" id="wrap">
				<div class="wrapper">
					<div class="button active-btn">
						<div class="icon">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<span>Home</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-user" aria-hidden="true"></i>
						</div>
						<span>About</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-file" aria-hidden="true"></i>
						</div>
						<span>Resume</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<span>Contact</span>
					</div>
				</div>  
			</div>		  
		</div>
	</nav>

	<!-- HOME -->
	<section id="my-home" class="d-flex flex-column justify-content-center">
		<div class="container home">
			<p>Hi There, I`m</p>
			<h1>Yoshua Ang - 160418032</h1>
			<h5>Fullstack Developer</h5>	
		</div>
	</section>

	<!-- PROFILE -->
	<section id="my-profile">
		<div class="container profile">
			<div class="title">
				<h2>About</h2>
				<h1>ME</h1>
			</div>
			<div class="card">
				<div class="profile-title">
					<img src="" alt="">
					<ul>
						<li><a href=""></a></li>
						<li><a href=""></a></li>
						<li><a href=""></a></li>
					</ul>
				</div>
				<div class="profile-main">
					<h2 class="profile-name"></h2>
					<p class="profile-body"></p>
				</div>
			</div>
		</div>
	</section>
	
</body>
	<script src="js/jquery/jquery.slim.min.js"></script>
	<script src="js/popper/umd/popper.min.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/my-jquery.js"></script>
</html>

<!-- ==========
	WRAPPER DEFAULT
	========= -->
<!-- <div class="wrapper" id="wrapp">
	<div class="button" style="float: left;">
		<div class="icon">
			<i class="fa fa-home" aria-hidden="true"></i>
		</div>
		<span>Home</span>
	</div>
	<div class="button">
		<div class="icon">
			<i class="fa fa-user" aria-hidden="true"></i>
		</div>
		<span>About</span>
	</div>
	<div class="button">
		<div class="icon">
			<i class="fa fa-envelope" aria-hidden="true"></i>
		</div>
		<span>Contact</span>
	</div>
</div>   -->